# Copyright (c) 2021 The Tor Project, inc. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

module TorPlugin
  # Returns a single random sample from the values found in array, but exclude
  # any values found in exclude_array.
  def random_sample(array, exclude_array=[])
    value = array - exclude_array
    value.sample
  end

  # Returns a random reviewer found in the reviewers argument, but exclude the
  # author of the MR.
  def random_reviewer(reviewers)
    random_sample(reviewers, [author[:username]])
  end

  # Returns the author of a given issue or MR.
  def author
    resource[:author]
  end

  # Returns the assignee of a given issue or MR.
  def assignee
    resource[:assignee]
  end

  # Returns true if the given issue or MR has an assignee.
  def has_assignee?
    !assignee.nil?
  end

  # Returns the list of reviewers.
  def reviewers
    resource[:reviewers]
  end

  # Returns true iff the given MR have a reviewer.
  def has_reviewer?
    !reviewers.empty?
  end

  # Show the resource data and return value as success indicator.
  def debug(value)
    puts "=== DEBUG ==="
    puts "#{resource}"
    puts "============="

    value
  end
end

Gitlab::Triage::Resource::Context.include TorPlugin
